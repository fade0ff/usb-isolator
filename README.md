2.5kV USB2.0 Isolator with 400mA supply and full speed (12Mbit/s) support

See http://lemmini.de/USB%20Isolator/USB%20Isolator.html

---------------------------------------------------------------
Copyright 2015 Volker Oth - VolkerOth(at)gmx.de

Licensed under the Creative Commons Attribution 4.0 license
http://creativecommons.org/licenses/by/4.0/

Everything in this repository is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OF ANY KIND, either express or implied.